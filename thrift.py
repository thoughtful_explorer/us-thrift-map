#!/usr/bin/python3

import requests
from bs4 import BeautifulSoup
import re
import json
import simplekml

#Set filename/path for KML file output
kmlfile = "usthrift.kml"
#Set KML schema name
kmlschemaname = "usthrift"
#Set page URL
pageURL = "https://www.thriftstores.net/state/index.php"
#Create list of states
states = ["AL","AK","AZ","AR","CA","CO","CT","DC","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"]

#Returns reusable BeautifulSoup of the site
def getsoupinit(state,page=1):
    #Start a session with the given page URL
    session = requests.Session()
    page = session.get(pageURL+"?st="+state+"&page="+str(page))
    #Return soupified HTML
    return BeautifulSoup(page.content, 'html.parser')

#Returns the number of HTML pages of store data associated with the given state
def getpagenum(state):
    #Initialize the soup
    soup = getsoupinit(state)
    #The number of store pages is contained within numbered links under the pagination div class
    try:
        pagelinks = soup(class_="pagination")[0].find_all("a")
    #But if there are no pages, the specified list call to the soup will retuen an IndexError so in that case we must return a value of 1 to represent 1 page
    except IndexError:
        return 1
    #Otherwise, return the number of pages as an integer, which is always the second to last link under the pagination div
    return int(pagelinks[len(pagelinks)-2].get_text())

#Returns a list of all the store locations as GeoJSON for a given state
def getstate(state):
    #Initialize collector list
    collector=[]
    #Loop through all the pages of this state
    for i in range(1,getpagenum(state)+1):
        #Initialize the soup
        soup = getsoupinit(state,i)
        #Isolate the store data - they are the only ones within the <body> tag that also have a type attribute set to application/javascript
        stores = soup.body.find_all("script",{"type":"application/javascript"})
        #Create dictionary of bad JSON values like single quotes -> double quotes  to make stores data into valid JSON and convert & to &amp; to make KML/XML valid
        dictionary = {": '":": \"",": '":": \"","',":"\",","'\n":"\"\n","type:":"\"type\":","features:":"\"features\":","title:":"\"title\":","description:":"\"description\":","properties:":"\"properties\":","geometry:":"\"geometry\":","coordinates":"\"coordinates\"","\\'":"'","&":"&amp;"}
        #Loop through the stores on this page
        for store in stores:
            #Convert the BeautifulSoup store object into a string and cut off the non-JSON stuff (27 chars at the beginning, 11 at the end)
            store = str(store.contents[0])[27:-11]
            #Loop through the correction dictionary to make all the needed formatting corrections
            for key in dictionary.keys():
                store = store.replace(key, dictionary[key])
            #Make a JSON object out of this store...
            jsonpoint = json.loads(store)
            #...and get its features only
            jsonpoint = jsonpoint["features"][0]
            #Append this store's JSON (it's really GeoJSON at this point) features to the collector list for this state
            collector.append(jsonpoint)
    return collector

#Returns a list of all the store locations as GeoJSON for all the states
def getallstates():
    #Initialize collector list
    collector=[]
    #Loop through all the states
    for state in states:
        #Append (or technically, extend) the collector list with this state's store locations
        collector.extend(getstate(state))
    return collector

#Saves a KML file of a given list of stores
def createkml(stores):
    #Initialize kml object
    kml = simplekml.Kml()
    #Add schema, which is required for a custom address field
    schema = kml.newschema(name=kmlschemaname)
    schema.newsimplefield(name="address",type="string")
    #Iterate through datamap for each store
    for store in stores:
        #Get the name of the store
        storename = store["properties"]["title"]
        #Get the address of the store, which is the second p tag in the div
        storeaddress = store["properties"]["description"]
        #Get coordinates from the geocode
        lat = store["geometry"]["coordinates"][1]
        lng = store["geometry"]["coordinates"][0]
        #First, create the point name and description in the kml
        point = kml.newpoint(name=storename,description="thrift store")
        #Then, add the custom "SimpleData" address field to kml file with proper hierarchical kml data structure
        point.extendeddata.schemadata.schemaurl = kmlschemaname
        simpledata = point.extendeddata.schemadata.newsimpledata("address",storeaddress)
        #Finally, add coordinates to the feature
        point.coords=[(lng,lat)]
    #Save the final KML file
    kml.save(kmlfile)
#Bring it all together - createkml(getstate("NY") can be used for individual states as well
createkml(getallstates())
