# US Thrift Store Location Extractor
Extracts Thriftstores.net (US thrift store information aggregator) locations from the official website, and places them into KML format.

## Dependencies
* Python 3.x
    * Requests module for session functionality
    * Beautiful Soup 4.x (bs4) for scraping
    * Regular expressions module for cleaning up messier-than-normal text
    * JSON module for JSON-based geodata
    * Simplekml for easily building KML files
* Also of course depends on official [Thriftstores.net](https://www.thriftstores.net/state/index.php) website.
